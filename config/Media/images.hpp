

	titles[]={};

	class customIntro01

	{

	  	idd=-1;

	  	movingEnable=0;

	  	duration=4;

	  	fadein=1;

	  	fadeout=1.9;

	  	name="customIntro01";

	  	controls[]={"title1"};



	  	class customTitle01

		{

			type=0;

			idc=-1;

			size=1;

			colorBackground[]={0,0,0,0};

			colorText[]={1,1,1,1};

			font="PuristaMedium";

	  	  	text="media\images\intro\introLogo2.jpg";

			style = 48 + 0x800; // ST_PICTURE + ST_KEEP_ASPECT_RATIO

	  	  	sizeEx=1;

			x=0.25 * safezoneW + safezoneX;

	  	  	y=0.25 * safezoneH + safezoneY;

	  	  	w=0.5 * safezoneW;

	  	  	h=0.5 * safezoneH;

		};

	};
