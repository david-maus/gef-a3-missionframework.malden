








// All Headgear to use as Gasmask

INS_gasMaskH = [
                "H_CrewHelmetHeli_B",
                "H_CrewHelmetHeli_O",
                "H_CrewHelmetHeli_I",
                "H_PilotHelmetFighter_B",
                "H_PilotHelmetFighter_O",
                "H_PilotHelmetFighter_I"
                ];

// All Goggles to use as Gasmask
//Hidden Identity Pack V.3
INS_gasMaskG = [
                "Mask_M50",
                "Mask_M40",
                "Mask_M40_OD"
                ];
